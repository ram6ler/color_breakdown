import 'dart:html';
import 'package:setalpha/setalpha.dart';

void main() {
  // Add interactivity to typed input.
  querySelector("#get_info").onClick.listen((_) {
    updateDisplay((querySelector("#color_choice") as InputElement).value);
  });

  // Add interactivity to color picker.
  var colorPick = querySelector("#color_choice_pick") as InputElement;

  colorPick.onInput.listen((_) {
    updateDisplay(colorPick.value);
  });

  // Add interactivity to the color display tabs.
  for (var id in ["nearest", "similar"]) {
    for (var i = 1; i <= 3; i++) {
      var element = querySelector("#$id-$i");
      element
        ..style.cursor = "pointer"
        ..onClick.listen((_) {
          updateDisplay(element.innerHtml);
        });
    }
  }

  // Initialize alpha cavas.
  var canvas = (querySelector("#alpha_illustrator") as CanvasElement);
  canvas
    ..width = 350
    ..height = 25
    ..style.cursor = "pointer";

  // Add interactivity to alpha canvas.
  canvas.onClick.listen((e) {
    var alpha = e.offset.x / canvas.width;
    updateDisplay(setAlpha(colorPick.value, alpha));
  });

  // Initialize with an arbitrary color.
  updateDisplay("hsla(120deg,50%,50%,0.25)");
}

void updateDisplay(String color) {
  // Check if valid.
  var errorElement = querySelector("#error_message");
  String hexColor;
  try {
    hexColor = setAlpha(color);
  } on Exception catch (_) {
    errorElement.innerHtml = "Unrecognized color: $color";
    return;
  }
  errorElement.innerHtml = "";

  // Update input values.
  (querySelector("#color_choice") as TextInputElement).value = color;
  (querySelector("#color_choice_pick") as InputElement).value =
      hexColor.substring(0, 7);

  // The luma, for determining label color.
  var luma = ColorProperty.luma(color);

  // Update data.
  var settings = <String, num>{
    "red": ColorProperty.red(hexColor),
    "green": ColorProperty.green(hexColor),
    "blue": ColorProperty.blue(hexColor),
    "hue": ColorProperty.hueInDegrees(hexColor),
    "saturation": ColorProperty.saturationAsPercent(hexColor),
    "lightness": ColorProperty.lightnessAsPercent(hexColor),
    "alpha": ColorProperty.alpha(hexColor)
  };

  settings.forEach((id, value) {
    querySelector("#$id").innerHtml =
        value == null || value.ceil() == value.floor()
            ? value.toString()
            : value.toStringAsFixed(2);
  });

  // Helper for stating and displaying a color.
  void paint(Element element, String color, [bool showColors = true]) {
    element.style
      ..background = color
      ..color = luma < 0.5 ? Color.white : Color.black;
    if (showColors)
      element.innerHtml = color;
    else
      element.innerHtml = "";
  }

  // Helper for updating nearest-color tab displays.
  void paintAll(String id, List<String> colors, [bool showColors = true]) {
    for (var i = 0; i < colors.length; i++) {
      var color = colors[i], element = querySelector("#$id-${i + 1}");
      paint(element, color, showColors);
    }
  }

  // Update the nearest RGB-space tabs.
  var nearestRGB = colorsNearestRGB(hexColor);
  paintAll("nearest", nearestRGB);

  // Update the nearest HSL-space tabs.
  if (settings["hue"] != null) {
    paintAll("similar", colorsNearestHSL(hexColor));
  } else {
    paintAll("similar", [Color.white, Color.white, Color.white], false);
  }

  // Update the color selected tab.
  paint(querySelector("#color"), setAlpha(color).substring(0, 7));

  // Update the color as input info.
  var input = querySelector("#input");
  paint(input, color);
  input.innerHtml = color;
  if (settings["alpha"] < 0.5 || luma > 0.5)
    input.style.color = "black";
  else
    input.style.color = "white";

  // Illustrate the alpha level.
  var canvas = (querySelector("#alpha_illustrator") as CanvasElement),
      gradient = canvas.context2D.createLinearGradient(0, 0, canvas.width, 0);
  gradient
    ..addColorStop(0, setAlpha(hexColor, 0))
    ..addColorStop(1, setAlpha(hexColor, 1));

  canvas.context2D
    ..fillStyle = Color.white
    ..fillRect(0, 0, canvas.width, canvas.height)
    ..lineWidth = 1
    ..strokeStyle = Color.black
    ..beginPath();

  for (var i = 1; i < 6; i++) {
    var y = i * canvas.height / 6;
    canvas.context2D
      ..moveTo(0, y)
      ..lineTo(canvas.width, y);
  }
  canvas.context2D.stroke();

  var x = settings["alpha"] * canvas.width, d = canvas.height * 0.25;

  canvas.context2D
    ..fillStyle = gradient
    ..fillRect(0, 0, canvas.width, canvas.height)
    ..strokeStyle = Color.black
    ..fillStyle = Color.white
    ..beginPath()
    ..moveTo(x - d / 2, 0)
    ..lineTo(x + d / 2, 0)
    ..lineTo(x, d)
    ..closePath()
    ..fill()
    ..stroke()
    ..strokeStyle = Color.white
    ..fillStyle = Color.black
    ..beginPath()
    ..moveTo(x - d / 2, canvas.height)
    ..lineTo(x + d / 2, canvas.height)
    ..lineTo(x, canvas.height - d)
    ..closePath()
    ..fill()
    ..stroke();
}
